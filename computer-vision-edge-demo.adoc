= Computer Vision at the Edge Demo
Diego Alvarez
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples/
:imagesdir: images
:icons: font
:source-highlighter: prettify
:toc: left
:toclevels: 5

== Introduction

This demo delves into the world of computer vision at the https://developers.redhat.com/topics/edge-computing[edge]. Throughout different episodes, we will explore the process of setting up the Single Node OpenShift infrastructure to develop and train https://developers.redhat.com/topics/ai-ml[AI/ML] models and deploy them at the edge on top of MicroShift to perform real-time inference.

- How to install https://www.redhat.com/en/blog/meet-single-node-openshift-our-smallest-openshift-footprint-edge-architectures[Single Node OpenShift] on bare metal
- https://developers.redhat.com/products/red-hat-openshift-data-science/overview[Red Hat OpenShift AI] installation and setup
- Prepare and label custom datasets with Label Studio
- Model training in Red Hat OpenShift AI
- Deploy computer vision apps at the edge with MicroShift

== Demo

++++
<!--ARCADE EMBED START-->
<div style="position: relative; padding-bottom: calc(56.25% + 41px); height: 0; width: 100%;">
  <iframe src="https://demo.arcade.software/NGojwz3jTGIWoaMybFVu?embed&embed_mobile=inline&embed_desktop=inline&show_copy_link=true" 
          title="Computer Vision at the Edge" 
          frameborder="0" 
          loading="lazy" 
          webkitallowfullscreen 
          mozallowfullscreen 
          allowfullscreen 
          allow="clipboard-write" 
          style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;" >
  </iframe>
</div>
<!--ARCADE EMBED END-->
++++


== What's next
Red Hat offers free programs and trials to get you started.

--
* Try Red Hat OpenShift AI in the no-cost https://developers.redhat.com/products/red-hat-openshift-ai/download[Developer Sandbox]

* Explore https://developers.redhat.com/products/red-hat-openshift-ai/getting-started[Red Hat OpenShift AI learning paths]
--
== Provide feedback
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/computer-vision-edge-demo.adoc[issue or submitting a merge request against this Portfolio Architecture product in our GitLab repositories].




