= RAG LLM Document Generation Demo
Ishu Verma @IoT_ishu
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples/
:imagesdir: images
:icons: font
:source-highlighter: prettify
:toc: left
:toclevels: 5

_Some details will differ based on the requirements of a specific implementation but all Red Hat architectures generalize one or more successful deployments of a use case._

== Use case

This demo is based based on  https://github.com/validatedpatterns-sandbox/rag-llm-gitops[*following*] validated pattern. It deploys a Chatbot application that leverages the power of Large Language Models (LLMs) in conjunction with the Retrieval-Augmented Generation (RAG) framework running on Red Hat OpenShift to generate a project proposal for a given Red Hat product.

== Background

LLMs are highly sophisticated AI models that are designed to understand and generate
human-like text, enabling a wide range of natural language processing applications. One
limitation of LLMs is that once they are generated, they do not have access to information beyond the date that they were trained. Retrieval Augmented Generation (RAG) can extend the functionality of the LLMs by retrieving facts from an external knowledge base, in this case, from a Redis in-memory Vector database.
Chat bots are designed to assist users by answering questions and processing
simple tasks. Answers remain up to date and contain information unique to the
organization by anchoring the model with relevant documentation.
In this solution, we have deployed a LLM based chat bot that can answer user
queries related to domain specific documents. As text-based searches are limited in
obtaining the right data, a digital assistant can help retrieve more accurate and relevant results using semantic search and natural language processing.

== Solution overview

[TODO: Add details]


--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/rag-demo-vp-marketing-slide.png[alt="RAG demo with Red Hat OpenShift", width=700]
--


_Figure 1. Overview of the RAG Demo with Red Hat OpenShift validated pattern ._


== Summary video

video::??[youtube]


== Logical diagram

--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/logical-diagrams/rag-demo-vp-ld.png[alt="RAG demo logical diagram", width=700]
--
_Figure 2. Logical diagram of the RAG demo._

== The technology

The following technology was chosen for this solution:

====
https://www.redhat.com/en/technologies/cloud-computing/openshift/try-it?intcmp=7013a00000318EWAAY[*Red Hat OpenShift*] is a unified platform to quickly build, modernize, and deploy both traditional and cloud-native applications at scale. It is packaged with a complete set of services for bringing apps to market on your choice of infrastructure. It’s based on an enterprise-ready Kubernetes container platform built for an open hybrid cloud strategy. It provides a consistent application platform to manage hybrid cloud, public cloud, and edge deployments. https://www.redhat.com/en/technologies/cloud-computing/openshift/ocp-self-managed-trial?intcmp=7013a000003Sh3TAAS[*Try It >*]

https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux?intcmp=7013a00000318EWAAY[*Red Hat Enterprise Linux*] is the world’s leading enterprise Linux platform. It’s an open source operating system (OS). It’s the foundation from which you can scale existing applications—and roll out emerging technologies—across bare-metal, virtual, container, and all types of cloud environments. https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux/server/trial?intcmp=7013a000003Sh3TAAS[*Try It >*]


====


== Architectures

=== RAG Demo Overview
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/rag-demo-vp-sd.png[alt="RAG with Red Hat OpenShift", width=700]
--

_Figure 3. Schematic diagram for Overview of RAG demo with Red Hat OpenShift._

[Add description]

==== RAG Data Ingestion
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/rag-demo-vp-ingress-sd.png[alt="RAG Demo with Red Hat OpenShift", width=700]
--

_Figure 4. Schematic diagram for Ingestion of data for RAG._


[Add description]


==== RAG Augmented Query

--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/rag-demo-vp-query-sd.png[alt="RAG demo augmented query", width=700]
--
_Figure 5. Schematic diagram for RAG demo augmented query._


[Add description]


In Figure 5, we can see RAG augmented query. Llama 2 model is used for for language processing, LangChain to
integrate different tools of the LLM-based application together and to process the PDF
files and web pages, Redis is used to store vectors, HuggingFace TGI is used to serve the Llama 2 model, Gradio is used for user interface and object storage to store language model and other datasets. Solution components are deployed as microservices in the Red Hat OpenShift cluster.


== Download diagrams
View and download all of the diagrams above in our open source tooling site.
--
https://www.redhat.com/architect/portfolio/tool/index.html?#gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/diagrams/rag-demo-vp.drawio[[Open Diagrams]]
--

== Provide feedback
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/ms-aro.adoc[issue or submitting a merge request against this Red Hat Architecture product in our GitLab repositories].

The opinions expressed on this website are those of the individual authors and do not necessarily reflect the views of their employer or Red Hat. The content published on this site is contributed by the community and is for informational purposes only. It is not intended to be, and should not be considered as, official Red Hat documentation, support, or advice.
