= Red Hat Ansible Automation Platform Service on AWS Demo
Hicham Mourad
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples/
:imagesdir: images
:icons: font
:source-highlighter: prettify
:toc: left
:toclevels: 5

== Introduction
Let's walk through how to get started provisioning your Red Hat Ansible Automation Platform Service on AWS instance.

== Demo

++++
<!--ARCADE EMBED START-->
<div style="position: relative; padding-bottom: calc(56.25% + 41px); height: 0; width: 100%;">
  <iframe src="https://demo.arcade.software/Xe9tI8rUVc2JrEI5LprL?embed&embed_mobile=inline&embed_desktop=inline&show_copy_link=true" 
          title="Red Hat Ansible Automation Platform Service on AWS Demo" 
          frameborder="0" 
          loading="lazy" 
          webkitallowfullscreen 
          mozallowfullscreen 
          allowfullscreen 
          allow="clipboard-write" 
          style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;" >
  </iframe>
</div>
<!--ARCADE EMBED END-->
++++


== What's next
Red Hat offers free programs and trials to get you started.

--
* Start automating with Red Hat Ansible Automation Platform on AWS. https://www.redhat.com/en/technologies/management/ansible/aws[Get started today].
--



== Provide feedback
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/ansible-aws-demo.adoc[issue or submitting a merge request against this Portfolio Architecture product in our GitLab repositories].




