= Industrial Edge
Ishu Verma  @ishuverma, William Henry @ipbabble,
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify
:toc: left
:toclevels: 5

_Check out the Red Hat  Validated Pattern for example application code to deploy this pattern for your own edge solution._

== Use case

Boosting manufacturing efficiency with artificial intelligence/machine learning (AI/ML) at the edge.

== Background


AI/ML can help industrial companies with tasks like predictive maintenance, quality control, and process automation. By inferencing on real time data at the factory floor, companies can reduce downtime and maintenance costs, improve quality and boost manufacturing efficiency.

--
Predictive maintenance: AI/ML models analyze data from sensors on equipment to predict failures before they occur, reducing downtime and maintenance costs.
Quality Control: Overview: AI-powered vision systems inspect products in real-time to detect defects and ensure quality standards.
Process Optimization:Overview: Real-time analysis and optimization of manufacturing processes to improve efficiency, reduce waste, and enhance product quality.
Robotics and Automation:

Overview: Intelligent robots and automated systems that perform tasks such as assembly, packaging, and material handling.

Model serving at the edge extends the deployment of AI models to remote locations using single-node OpenShift. It provides inferencing capabilities in resource-constrained environments with intermittent or air-gapped network access.
--

== Solution overview


image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/edge-manufacturing-efficiency-marketing-slide.webp[alt="High level view of Industrial Edge", width=700]

_Figure 1. Industrial edge solution overview._


Figures 1 and 2 provide an overview of the industrial edge solution. It is applicable across a number of verticals including manufacturing.

This solution:

- Enables AI/ML workflow from data gathering to model training and serving
- Provides tooling for AI/ML model training, deployment and lifecycle
- Automates GitOps and DevOps management across core and factory sites

This architecture can be distributed across two logical types of sites: the core datacenter and the factories.

- **The core datacenter**. This is where data scientists, developers, and operations personnel train models, create application code, and configurations.
- **The factories**. This is where models, applications, updates, and operational changes are deployed to improve quality and efficiency in the factory.

image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/edge-manufacturing-vp.png[alt="Data Flows in Industrial Edge solution", width=700]

_Figure 2. Overall data flows of solution._

Figure 2 provides a different high-level view of the solution with a focus on the two major dataflow streams.

The first stream is about moving sensor data and events from the operational/shop floor edge towards the core. In Figure 2, this stream goes from left to right.

The second stream is about pushing code, configuration, master data, and machine learning models from the core (where development, testing, and training is happening) towards the edge / shop floors. In Figure 2, this is from right to left.


== Logical diagrams

image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/logical-diagrams/industrial-edge-ld.png[alt="Conceptual view of Industrial Edge deployed at various locations", width=700]

_Figure 3: Industrial Edge solution as logically and physically distributed across multiple sites._

The following technology was chosen for this solution as depicted logically in Figure 4.

== Architectures

=== Edge manufacturing with messaging and ML

image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/edge-mfg-devops-data-sd.png[alt="Data interaction of various Industrial Edge components", width=700]

_Figure 4: Industrial Edge solution showing messaging and ML components schematically._

As shown in Figure 4, data coming from sensors is transmitted over MQTT (Message Queuing Telemetry Transport) to Red Hat AMQ, which routes sensor data for two purposes: model development in the core data center and live inference in the factory data centers.

Red Hat Integration - Camel K is a lightweight integration framework that runs natively on OpenShift, provides the MQTT integration that normalizes and routes sensor data to the other components.

The sensor data is mirrored into a data lake that is provided by Red Hat OpenShift Data Foundation.

Data scientists then use tools and libraries like Jupyter, TensorFlow, and PyTorch from OpenShift AI to perform model development and training.

Once the models have been tuned and are deemed ready for production, the artifacts are committed to git which kicks off an image build of the model using OpenShift Pipelines (based on the upstream Tekton), a serverless CI/CD system that runs pipelines with all the required dependencies in isolated containers.

Operations team use OpenShift AI MLOps components for model serving and data science pipelines.

The model image is pushed into OpenShift’s  integrated registry and can be pulled by factory datacenter for real time inferencing.

image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/edge-mfg-devops-network-sd.png[alt="Using network segragation to protect factories and operations infrastructure from cyber attacks", width=700]

_Figure 5: Industrial Edge solution showing network flows schematically._

As shown in Figure 5, in order to protect the factories and operations infrastructure from cyber attacks, the operations network needs to be segregated from the enterprise IT network and the public internet. The factory machinery, controllers, and devices need to be further segregated from the factory data center and need to be protected behind a firewall.

=== Edge manufacturing with GitOps

image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/edge-mfg-gitops-sd.png[alt="Using GitOps for managing any changes to clusters and applications", width=700]

_Figure 6: Industrial Edge solution showing a schematic view of the GitOps workflows._

GitOps is an operational framework that takes DevOps best practices used for application development such as version control, collaboration, compliance, and CI/CD, and applies them to infrastructure automation. Figure 6 shows how, for these industrial edge manufacturing environments, GitOps provides a consistent, declarative approach to managing individual cluster changes and upgrades across the centralized and edge sites. Any changes to configuration and applications can be automatically pushed into operational systems at the factory.

### Secrets exchange and management

image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/edge-mfg-security-sd.png[alt="Secret exchange and management", width=700]

_Figure 7: Schematic view of secrets exchange and management in an Industrial Edge solution._

Authentication is used to securely deploy and update components across multiple locations. The credentials are stored using a secrets management solution like Hashicorp Vault. The external secrets component is used to integrate various secrets management tools (AWS Secrets Manager, Google Secrets Manager, Azure Key Vault). As shown in Figure 7, these secrets are then passed to Red Hat Advanced Cluster Management for Kubernetes (RHACM) which pushes the secrets to the RHACM agent at the edge clusters based on policy. RHACM is also responsible for providing secrets to OpenShift for GitOps workflows (using Tekton and Argo CD).


== Demo scenario

This scenario is derived from the https://github.com/sa-mw-dach/manuela[MANUela work] done by Red Hat Middleware Solution Architects in Germany. The name MANUela stands for MANUfacturing Edge Lightweight Accelerator, you will see this acronym in a lot of artifacts. It was developed on a platform called https://github.com/stormshift/documentation[stormshift].

The demo was subsequently with an advanced GitOps framework.

image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/highleveldemodiagram-vp.png[alt="The specific example is machine condition monitoring based on sensor data in an industrial setting, using AI/ML. It could be easily extended to other use cases such as predictive maintenance, or other verticals", width=700]

_Figure 8. High-level demo summary. The specific example is machine condition monitoring based on sensor data in an industrial setting, using AI/ML. It could be easily extended to other use cases such as predictive maintenance, or other verticals._

The demo scenario reflects the data flows described earlier and shown in Figure 3 by having three layers.

**Line Data Server:** the far edge, at the shop floor level.

**Factory Data Center:** the near edge, at the plant, but in a more controlled environment.

**Central Data Center:** the cloud/core, where ML model training, application development, testing, and related work happens. (Along with ERP systems and other centralized functions that are not part of this demo.)

The northbound traffic of sensor data is visible in Figure 8. It flows from the sensor at the bottom via MQTT to the factory, where it is split into two streams: one to be fed into an ML model for anomaly detection and another one to be streamed up to the central data center via event streaming (using Kafka) to be stored for model training.

The southbound traffic is abstracted  in the App-Dev / Pipeline box at the top. This is where GitOps kicks in to push config or version changes down into the factories.

Industrial edge pattern: https://validatedpatterns.io/patterns/industrial-edge/

== What's next
Red Hat offers free programs and trials to get you started.
--
* Explore containers, Kubernetes, and OpenShift in the https://developers.redhat.com/developer-sandbox[Developer Sandbox] for free, no setup required.
* Try https://developers.redhat.com/products/integration/getting-started[Red Hat AMQ] with Red Hat Integration
* Download https://developers.redhat.com/products/rhel/overview[Red Hat Enterprise Linux]
* Interactive Labs for hands-on, step-by-step lessons on products and technologies.
** https://play.instruqt.com/embed/redhat/tracks/getting-started-edge-lab?token=em_VX7rLMJ7-5Hf8WoX&show_challenges=true[Getting started with Ansible Automation Platform and edge]
** https://www.redhat.com/en/interactive-labs/edge-management-red-hat-enterprise-linux[Edge Management Lab]
--

== Provide feedback on this architecture
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/industrial-edge.adoc[issue or submitting a merge request against this Red Hat Architecture product in our GitLab repositories].
