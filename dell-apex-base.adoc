= Dell APEX Cloud Platform with Red Hat OpenShift
Ishu Verma @IoT_ishu
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples/
:imagesdir: images
:icons: font
:source-highlighter: prettify
:toc: left
:toclevels: 5

_Some details will differ based on the requirements of a specific implementation but all Red Hat architectures generalize one or more successful deployments of a use case._

== Use case

Dell APEX Cloud Platform enables on-premise OpenShift, while simplifying the process of deploying and managing the underlying infrastructure. This gives customers the ability to meet regulatory and data-locality requirements, while still providing an experience similar to consuming OpenShift in a public cloud.

== Background

The Dell APEX Cloud Platform for Red Hat OpenShift introduces a new level of integration for running OpenShift on bare metal servers. Until now, all management of the infrastructure has been through separate original equipment manufacturer (OEM)
tooling, handled separately from the management of OpenShift itself. This increases the staff and skillset requirements to maintain the system, thus increasing the cost and complexity of such a solution.

For example, separate staff has been required to test BIOS, firmware, and driver updates as they became available followed by a maintenance period to apply those updates. Dell’s Cloud Platform Foundation software can expose the infrastructure management into the OpenShift Web Console, allowing administrators to update the hardware using the same workflow that they use to update the OpenShift software. This allows the OpenShift administrators to manage the infrastructure with the same tooling they use to manage the cluster and the applications running on it, freeing maintenance teams for other tasks. This, combined with Dell's Continuously Validated State, means that
each update to the environment is thoroughly tested to ensure that all of the various components throughout the stack work together.

== Solution overview

APEX Cloud Platform uses a two-layer architecture, with storage and compute separated for optimal usage. The architecture has two separate groups of nodes:

- Compute nodes
- Storage nodes

OpenShift is deployed on the compute nodes. To ensure resiliency of the control plane, a minimum of four compute nodes are required. OpenShift is deployed on the bare metal on the compute nodes i.e. it's deployed directly on the hardware without the need for a hypervisor. This deployment takes  advantage of all the OpenShift features including OpenShift Virtualization for running VMs and Knative for serverless compute.

The storage nodes run the Dell Software Defined Storage (SDS) software on Red Hat Enterprise Linux (RHEL) directly on the storage nodes. The management components related to Dell SDS are co-resident on the storage nodes and do not consume resources on the compute nodes.
Currently, there is basic integration between OpenShift and the Dell SDS Element Manager. In future releases, primary storage operations will be available directly in the OpenShift Web Console. This will include creating volumes, replacing disks, and adding or removing nodes from the storage cluster. This will also incorporate lifecycle management of the Dell SDS components into the same update workflow that maintains OpenShift, the Cloud Platform Foundation Software, and the infrastructure hardware components.

--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/dell-apex-marketing-slide.webp[alt="Dell APEX Cloud Platform with Red Hat OpenShift", width=700]
--

_Figure 1. Overview of the Dell APEX Cloud Platform with Red Hat OpenShift._


== Summary video
video::4GFxMVGAXaw[youtube]

== Logical diagram

As seen in Figure 2, this solution uses a combination of Red Hat OpenShift functionality and services from the Dell APEX Cloud Platform.

--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/logical-diagrams/dell-apex-base-ld.png[alt="Dell APEX Cloud Platform with Red Hat OpenShift generic components ", width=700]
--
_Figure 2. Logical diagram of the Dell APEX Cloud Platform with Red Hat OpenShift._

== The technology

The following technology was chosen for this solution:


https://www.redhat.com/en/technologies/cloud-computing/openshift/try-it?intcmp=7013a00000318EWAAY[*Red Hat OpenShift*] is a unified platform to quickly build, modernize, and deploy both traditional and cloud-native applications at scale. It is packaged with a complete set of services for bringing applications to market on your choice of infrastructure. It’s based on an enterprise-ready Kubernetes container platform built for an open hybrid cloud strategy. It provides a consistent application platform to manage hybrid cloud, public cloud, and edge deployments. https://www.redhat.com/en/technologies/cloud-computing/openshift/ocp-self-managed-trial?intcmp=7013a000003Sh3TAAS[*Try It >*] 


https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux?intcmp=7013a00000318EWAAY[*Red Hat Enterprise Linux*] is the world’s leading enterprise Linux platform. It’s an open source operating system (OS). It’s the foundation from which you can scale existing applications—and roll out emerging technologies—across bare-metal, virtual, container, and all types of cloud environments. https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux/server/trial?intcmp=7013a000003Sh3TAAS[*Try It >*]

[TODO: Add Dell platform and storage compoenents]




== Architectures

=== Dell APEX Cloud Platform with Red Hat OpenShift
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/dell-apex-base-sd.png[alt="Dell APEX Cloud Platform with Red Hat OpenShift", width=700]
--

_Figure 3. Schematic diagram for Dell APEX Cloud Platform with Red Hat OpenShift._

Figure 3 shows the physical architecture of the Apex platform with OpenShift. Dell APEX Cloud Platform for Red Hat OpenShift compute layer setup is configured with
four Dell APEX MC-760 nodes.

Dell APEX MC-760 server is a 2U, two-socket fully featured enterprise rack server, designed to optimize even the most demanding workloads like Artificial Intelligence (AI) and Machine Learning. It provides balanced CPU and storage for demanding workloads, powered by Intel Xeon processors
and Nvidia GPUs.

Dell APEX MC-760 Servers offer:

• Up to two Next Generation Intel Xeon Scalable processors with up to 56 cores for
faster and more accurate processing performance.
• Accelerate in-memory workloads with up to 32 DDR5 RDIMMS up to 4400
MT/sec (2DPC) or 4800 MT/sec for 1DPC (16 DDR5 RDIMMs max).
• Support for GPUs including 2 x double-wide or 6 x single-wide for workloads
requiring acceleration.

==== Dell APEX Cloud Platform with Red Hat OpenShift cluster lifecycle management

--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/apex-lifecycle-sd.png[alt="Dell APEX Cloud Platform with Red Hat OpenShift Lifecycle Management", width=700]
--
_Figure 4. Schematic diagram for cluster lifecycle management._

In Figure 4, we can see cluster lifecycle management with Cloud Foundation Platform Software.

==== RAG with Red Hat OpenShift AI and Dell APEX Cloud Platform

--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/dell-apex-rag-sd.png[alt="Retrieval Augmented Generation with OpenShift AI and Dell APEX Cloud Platform", width=700]
--
_Figure 4. Schematic diagram for Retrieval Augmented Generation (RAG) with OpenShift AI on Dell APEX Cloud Platform._

Large Language Models (LLMs) are sophisticated AI models that are designed to understand and generate
human-comprehensible text, enabling a wide range of natural language processing applications. One
limitation of LLMs is that, once they are generated, they do not have access to information beyond the date that they were trained. Retrieval Augmented Generation (RAG) can extend the functionality of the LLMs by retrieving facts from an external knowledge base, in this case, from a Redis in-memory Vector database.

Chatbots are designed to assist users by answering questions and processing
simple tasks. Answers remain up to date and contain information unique to the
organization by anchoring the model with relevant current documentation.
In this solution, we have deployed a LLM-based chatbot that can answer user
queries related to domain-specific documents. As text-based searches are limited in
obtaining the right data, a digital assistant can help retrieve more accurate and relevant results using semantic search and natural language processing.


In Figure 4, we can see a RAG use case deployed on Dell Apex Cloud Platform. Llama 2 model is used for language processing, LangChain to
integrate different tools of the LLM-based application together and to process the PDF
files and web pages, Redis to store vectors, Caikit to serve the Llama 2 model, and Gradio for the user interface and the object storage to store the language model and other datasets. Solution components are deployed as microservices in the Red Hat OpenShift cluster.

== What's next
Red Hat offers free programs and trials to get you started.
--
* Explore containers, Kubernetes, and OpenShift in the https://developers.redhat.com/developer-sandbox[*Developer Sandbox*] for free, no setup required.
* Download https://developers.redhat.com/products/rhel/overview[*Red Hat Enterprise Linux*]
--


== Provide feedback
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/dell-apex-base.adoc[issue or submitting a merge request against this Red Hat Architecture product in our GitLab repositories].

The opinions expressed on this website are those of the individual authors and do not necessarily reflect the views of their employer or Red Hat. The content published on this site is contributed by the community and is for informational purposes only. It is not intended to be, and should not be considered as, official Red Hat documentation, support, or advice.
