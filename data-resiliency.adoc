= Achieving Data Resiliency
Ricardo Garcia Cavero @rgarciac
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples/
:imagesdir: images
:icons: font
:source-highlighter: prettify
:toc: left
:toclevels: 5

_Some details will differ based on the requirements of a specific implementation but all Red Hat architectures generalize one or more successful deployments of a use case._

== Use case
 
This architecture showcases how Red Hat technologies empower businesses to achieve sovereign cloud compliance enabling rapid recovery and business continuity after external disruptions, like disasters, cyberattacks, or human error, leveraging a multi-cloud and automated approach.

== Background 

Businesses must be prepared to handle service interruptions caused by infrastructure and application failures, commonly managed with disaster recovery (DR), high availability (HA), and business continuity (BC) solutions, as well as external events like cyberattacks and human errors causing data loss.

The maturity of an infrastructure and application resilience program is essential for ensuring the continuity of critical services and protecting company assets.
In a geopolitical landscape marked by ongoing challenges, the European community is updating regulations to safeguard consumer data. Companies must be ready to adapt their services and infrastructure to comply with regulations such as *DORA* and *NIST*.
Technical capabilities to enhance resilience at all levels focus on:

. *Multi-Cloud and technology enablement:* manage and enable infrastructure and data resiliency in multiple scenarios, from onprem and hybrid to multi - cloud. 
. *Data Protection and Security:* Reducing the risk of system/data unavailability, protecting data from erasure/destruction, lowering the recovery point objective (RPO), and reducing the recovery time objective (RTO) from cyberattacks and data breaches. Security measures include environment isolation, air-gapped networks, write-once-read-many (WORM) storage, and immutable datasets to prevent malicious activities or errors.
. *Recovery and Incident Management:* Tools and features provided by vendors to manage and recover from incidents.
. *Enforcement and Audit of Policies:* Automation and customization of out-of-the-box policies to guarantee RPO and RTO based on data/application criticality, with alarms triggered when violations occur.

By leveraging the Red Hat technologies described in this architecture, engineered and implemented by ACIC Rome (Accenture Cloud Innovation Center) and Accenture Sovereign Cloud Practice teams, customers can address these requirements by enabling customizable services that ensure compliance with European and local regulations, especially addressing potential security and data protection needs.

== Solution overview

The implemented solution covers the following objectives:

* *Business Continuity / Disaster Recovery:* Ensures operations continue during disasters that affect production infrastructure through Disaster Recovery solutions.
* *Cyber Resiliency:* Uses offline backups with Red Hat or third-party tools to protect against cyber threats.

In *both scenarios*, automation will be used to automatically manage steps like the setup of Red Hat OpenShift environments, or to enable the automatic restore of production environments and services in cloud environments after a cyber-attack.


=== How to enable a Disaster Recovery scenario with Red Hat

A two-site scenario (Primary and Secondary) is deployed. Data replication between sites is allowed leveraging Red Hat Openshift Regional-DR capability on the production site with persistent volume data replication.

This configuration ensures service restart in the event of *infrastructure* failure at the primary site.

The main components of the solution are:

* *Red Hat Advanced Cluster Management (RHACM):* offers the capability to manage multiple clusters, policies, and application lifecycles. In this scenario:
** Enables data management policies allowing to define data replication interval and control data loss rates (RPO) between primary and secondary clusters.
* *Red Hat Openshift Disaster Recovery:* This is an orchestrator that facilitates disaster recovery for stateful applications across a set of peers OpenShift clusters managed by RHACM. It provides cloud-native interfaces to:
** Fail over an application and its state to a peer cluster.
** Relocate an application and its state back to the previously deployed cluster.
* *Red Hat OpenShift Data Foundation:* This component allows for the provisioning and management of storage (and optionally encryption with KMS) for stateful applications in an OpenShift Container Platform cluster. In our solution is used to: 
** Store metadata required for volume failover and application recovery during a disaster.
** Automatically mirror images across storage pools 
* *Submariner:* Submariner ensures secure and efficient network connectivity between multiple OpenShift clusters across different regions. In this scenario:
** It connects the OpenShift service network between the production cluster and the disaster recovery. 
** It allows for secure volume data replication between the production cluster and disaster recovery.

=== How to enable a cyber resiliency  scenario with Red Hat

In the event of a *cyber-attack*, the Regional DR solution doesn’t guarantee data resiliency due to specific precautions that have not been implemented.

Disaster recovery environment, due to stringent replication policies, is an attackable environment and therefore is insufficient to guarantee protection against security attacks.

Backup tools can also be involved in cyber-attacks, allowing malware to overwrite backup copies.

This solution allows on-demand and automatic activation of a Cyber Vault site in the cloud. This site relies on an *offline backup* that can be used to recreate the entire production site in a completely separate secure environment, as well as guaranteeing a secure copy of data involved in the attack.

In addition to the component used to enable a Disaster Recovery scenario, the solution includes these components:

* *Ansible Automation Platform:* is a comprehensive solution for IT automation, enabling users to automate complex workflows. In this scenario:
** It enables the automatic creation and restoring of OpenShift production environments, configurations and applications following an Infrastructure as Code (IaC) approach.
** Manages the backup/restore processes, enabling air-gap capabilities, to minimize the risk of cyber-attacks.
* *OADP Operator:* This operator is used to backup and restore *application* persistent volumes. In this solution:
** The backup process leverages cloud provider features such as S3 buckets for *versioning, WORM lock, encryption with KMS, and retention lock* to ensure data protection and compliance.
** An *air gapped* network link between the Production site and the Offline Backup (S3 storage) is implemented, utilizing OADP post and pre-hooks. Network policies will be used to close outbound links, and Ansible will be utilized to close S3 ingress links.
* *Red Hat Advanced Cluster Security (RHACS):* is a Kubernetes-native security platform that helps to build, deploy, and run cloud-native applications with more security. In our solution it is used to:
** Monitor system-level events such as process execution and network connections to detect anomalous activity indicative of malicious intents such as active malware.
** It raises non-compliance alerts in case of attack based on non baselined network flows. 
* Third-party monitoring tool that monitors the database and raises alerts in a collaborative tool in case of suspicious activity (e.g. massive update of a database row)

The solution is customizable based on cloud provider costs and the required RTO. An on-demand, low-cost vault site can be offered to mitigate cyber risks and ensure secure data recoverability. For faster recovery, a pre-hosted ACM Vault site can be utilized, depending on the desired RTO and associated costs.




--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/data-resiliency-marketing-slide.webp[alt="Achieving Data Resiliency", width=700]

--
_Figure 1. Overview of Achieving Data Resiliency architecture_


== Summary video
video::mPDOjZKgLng[youtube]

== Logical diagrams


--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/logical-diagrams/data-resiliency-ld.png[alt="Generic components of platform to achieve data resiliency", width=700]

--
_Figure 2. The application platform with its different components_

== The technology

As stated before, these are the components that constitute this solution:

https://www.redhat.com/en/technologies/cloud-computing/openshift?intcmp=7013a00000318EWAAY[*Red Hat OpenShift*] https://www.redhat.com/en/technologies/cloud-computing/openshift/try-it?intcmp=7013a000002D1gVAAS[*Try It >*]

https://www.redhat.com/en/technologies/management/ansible?intcmp=7013a00000318EWAAY[*Red Hat Ansible Automation Platform*] https://www.redhat.com/en/technologies/management/ansible/trial?intcmp=7013a000003Sh3TAAS[*Try It >*]

https://www.redhat.com/en/technologies/management/advanced-cluster-management?intcmp=7013a00000318EWAAY[*Red Hat Advanced Cluster Management for Kubernetes (RHACM)*] https://www.redhat.com/en/technologies/management/advanced-cluster-management/trial?intcmp=7013a000003Sh3TAAS[*Try It >*]

https://www.redhat.com/en/technologies/cloud-computing/openshift/advanced-cluster-security-kubernetes?intcmp=7013a00000318EWAAY[*Red Hat Advanced Cluster Security for Kubernetes (RHACS)*] https://www.redhat.com/en/technologies/cloud-computing/openshift/advanced-cluster-security-kubernetes/cloud-service/trial?intcmp=7013a000003Sh3TAAS[*Try It >*]

https://www.redhat.com/en/blog/disaster-recovery-strategies-for-applications-running-on-openshift[*Red Hat OpenShift Disaster Recovery*] 

https://www.redhat.com/en/technologies/cloud-computing/openshift-data-foundation?intcmp=7013a00000318EWAAY[*Red Hat OpenShift Data Foundation*] https://www.redhat.com/en/technologies/cloud-computing/openshift/data-foundation/trial?intcmp=7013a000003Sh3TAAS[*Try It >*]

https://access.redhat.com/articles/5456281[*Red Hat OpenShift APIs for Data Protection (OADP)*]

https://docs.redhat.com/en/documentation/red_hat_advanced_cluster_management_for_kubernetes/2.2/html/manage_cluster/submariner[*Submariner*]



== Architectures

=== Data Resiliency Infrastructure (workflow) 



--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/data-resiliency-sd.png[alt="Achieving Data Resiliency workflow", width=700]

--
_Figure 3. Workflow of the solution to achieve data resiliency_

This workflow ensures that clients can effectively respond to both infrastructure faults and cyber-attacks, maintaining operational continuity and data integrity by leveraging a comprehensive suite of Red Hat and cloud tools.

==== Normal Operations

1. Openshift clusters are managed by Red Hat Advanced Cluster Management (RHACM) that perform application synchronization, cluster and policy management.
2. Openshift Disaster Recovery and Openshift Data Foundation enable asynchronous replication of persistent volumes between production and disaster recovery clusters.
3. Ansible Automation Platform and OpenShift API for Data Protection (OADP) automatically schedule backups to Amazon S3 with versioning and WORM (Write Once Read Many) policies to ensure data immutability. Leveraging on OADP pre and post hook we can manage an air gapped link with S3 bucket.


==== Infrastructure Fault

4. Trigger failover to the disaster recovery cluster using Red Hat Advanced Cluster Management (RHACM) leveraging OpenShift Disaster Recovery (ODR).

==== Cyber Attack

5. Utilize RHACS to detect suspicious activities and send alerts. 
6. Use Ansible Automation Platform to recreate the environment on AWS with configurations stored in the central repository.
7. Automatically restore uncompromised backups from S3 using OADP.


== Download diagrams
View and download all of the diagrams above in our open source tooling site.
--
https://www.redhat.com/architect/portfolio/tool/index.html?#gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/diagrams/data-resiliency.drawio[[Open Diagrams]]
--

== Provide feedback
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/data-resiliency.adoc[issue or submitting a merge request against this Portfolio Architecture product in our GitLab repositories].

The opinions expressed on this website are those of the individual authors and do not necessarily reflect the views of their employer or Red Hat. The content published on this site is contributed by the community and is for informational purposes only. It is not intended to be, and should not be considered as, official Red Hat documentation, support, or advice.

