= Virtualization Migration Solution
Ricardo Garcia Cavero @rgarciac
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples/
:imagesdir: images
:icons: font
:source-highlighter: prettify
:toc: left
:toclevels: 5


_Some details will differ based on the requirements of a specific implementation but all Red Hat architectures generalize one or more successful deployments of a use case._

== Use case

Consolidate different application platforms in one to host all the customer applications, those running on VMs and those running on containers and apply automation at scale to manage that platform. This greatly simplifies administration tasks and provides the agility and flexibility needed to boost developer productivity.

== Background

Application platform landscapes have been becoming more and more complex and heterogeneous over time and this translates into having to dedicate more resources to their management, as well as introducing task overhead and bottlenecks that stiffen the service flow and slow down the pace of innovation when developing new applications.

Modern development practices make extensive use of containerization techniques but there are still many applications that are not ready to be containerized and need to keep running on VMs. Thus having all of them running on a single platform means a radical change in the way systems are managed and if automation technologies are added to the equation the gain turns out to be huge, both in terms of the administrator’s time that is freed up so that they can devote it to more innovative tasks and the raise in productivity for the developers as they have at their disposal everything they need to start coding quickly, even taking advantage of self-service (of VMs and other components).

== Solution overview

Allowing for and ensuring high developer productivity should be the ultimate goal of any application platform and if its management is difficult there is always a risk that this productivity might not be optimal as the platform might not be always ready because of failures, or the tools for developers might take time to be usable because of task overload in its administration.

In Figure 1 we can see the main business drivers that lead customers to adopt this solution.

--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/ocp-virt-migration-marketing-slide.webp[alt="Virtualization Migration Solution", width=700]


--


_Figure 1. Overview of the Migration to Red Hat OpenShift Virtualization architecture_

Along with those business drivers, a very important point that helps customers decide on a single open source platform is to avoid vendor lock-in and be able to have their IT estate spread across a hybrid cloud, with again, ease of management and ensuring robust security through the whole lifecycle of the applications.

The initial step of the migration is the Virtualization Migration Assessment that takes place before the actual migration and analyzes the customers current VM estate and applications running on them in order to come up with a plan that best suits where customers want to be and at what pace, prescriptively defining the migration path and method to use. VM auto-discovery is also used during this assessment to speed up and simplify the process. Once the assessment is completed, migration tools will be used to migrate the VMs from their previous platform. The process will be orchestrated using automation. This minimizes the possibility of errors in the migration and speeds up the process dramatically as it is made repeatable.

The new platform is also managed using automation at scale to guarantee high availability and resiliency, making upgrades much easier and this automation is also applied to the application development process in the form of CI/CD practices.

== Summary video 

video::ml_EHfHj93A[youtube]

== Logical diagrams

In Figure 2 we can see on the left hand side what a classical virtualization platform might look like and then on the right hand side there is the platform after the migration. It will host the VMs (using KubeVirt technology) and applications that do not run on VMs. The platform also contains the tools to perform the VM migrations. Third party solutions are used to provide persistent storage, networking, backup/restore and Disaster Recovery to the VMs.

--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/logical-diagrams/ocp-virt-migration-ld.png[alt="Generic components of the Virtualization Migration Solution", width=700]

--


_Figure 2. Before and after the migration of the VMs to the new platform._

== The technology

https://www.redhat.com/en/technologies/cloud-computing/openshift/try-it?intcmp=7013a00000318EWAAY[*Red Hat OpenShift*] is the underlying platform where the VMs will be migrated to. It can also host applications that do not need to run on a VM. Both (the VMs and the other applications) can be deployed on any type of infrastructure, allowing customers to build and operate hybrid clouds with ease. It also provides the tools to manage with automation the lifecycle of the applications, applying CI/CD practices.
Red Hat Migration Toolkit for Virtualization provides all the necessary tools to migrate VMs from a source platform to Red Hat OpenShift. https://www.redhat.com/en/technologies/cloud-computing/openshift/ocp-self-managed-trial?intcmp=7013a000003Sh3TAAS[*Try It >*]

https://www.redhat.com/en/technologies/management/ansible?intcmp=7013a00000318EWAAY[*Red Hat Ansible Automation Platform*] is used to orchestrate the migration of the VMs from their previous platform and once migrated, to manage them with automation through their entire lifecycle.
Red Hat Ansible Automation Hub contains all the automation assets used by Red Hat Ansible Automation Platform for the management, in order to maintain the desired state of all the components. https://www.redhat.com/en/technologies/management/ansible/trial?intcmp=7013a000003Sh3TAAS[*Try It >*]

https://www.redhat.com/en/technologies/cloud-computing/openshift/openshift-ai?intcmp=7013a00000318EWAAY[*Red Hat OpenShift Virtualization*], based on KubeVirt technology, provides all the components needed to create VMs and administrate them with ease using a graphical interface (it also has a command line). It supports third-party solutions for adding persistent storage, networking, backup/restore, and Disaster Recovery capabilities to the VMs. https://www.redhat.com/en/technologies/cloud-computing/openshift/openshift-ai/trial?intcmp=7013a000003Sh3TAAS[*Try It >*]

https://www.redhat.com/en/technologies/management/advanced-cluster-management?intcmp=7013a00000318EWAAY[*Red Hat OpenShift Advanced Cluster Management for Kubernetes*] monitors the state of all the components deployed on Red Hat OpenShift, namely the VMs, gathering information that it sends to Ansible Automation Platform so that corrective actions can be taken in case they are needed. https://www.redhat.com/en/technologies/management/advanced-cluster-management/trial?intcmp=7013a000003Sh3TAAS[*Try It >*]


== Architecture

In this architecture we want to highlight the components and process of actual migration but also show how the target platform looks once the migration has been completed and how its components allow for ease of management.

=== Migration 

--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/ocp-virt-migration-sd1.png[alt="Migration using the Virtualization Migration Solution", width=700]

--

_Figure 3. Migration from source virtualization platform using the Virtualization Migration Solution_

In this solution the target container platform is Red Hat OpenShift (although it could also be Red Hat OpenStack or Red Hat Enterprise Linux on KVM), in which the OpenShift Virtualization operator has been installed. This operator allows for the creation and management of VMs. They can be deployed and added to OpenShift projects exactly as containerized applications are.
Another Red Hat OpenShift operator involved in the solution is the Migration Toolkit for Virtualization. With it, connections can be created to the source VMs to get their configuration and they will be replicated on Red Hat Openshift in a straightforward way and really fast. Both operators are included with any Red Hat OpenShift subscription.

To perform a migration at scale, Ansible Automation Platform completes the picture. It creates an inventory with all the source VMs that need to be migrated, as well as the network devices that they use and allows customers to schedule the migration in different batches (according to their maintenance windows) with the Migration Toolkit for Virtualization. Once the VMs are on Red Hat OpenShift it runs post-migration tasks to make sure that all the new VMs are tuned to perform optimally.

=== Steady State

--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/ocp-virt-migration-sd2.png[alt="Steady state management with automation of the target platform after migration", width=700]

--

__Figure 4. Management of the platform with automation__

After the VMs have been migrated the longest part of their life cycle begins. It is the Steady State that lasts till their end of life and all the management operations involved in it tend to be called Day 2 Operations. Red Hat OpenShift Console offers a GUI from where the VMs can be easily managed, performing start/stop operations, live migrations, cloning them, taking snapshots, adding/deleting storage and network interfaces, etc. Besides this, Red Hat’s rich partner ecosystem gives customers great flexibility when choosing solutions for persistent storage for the VMs, their networking configuration and operation as well as for backup/restore and Disaster Recovery strategies.

As for any other workload, automation is of the utmost importance in the management of VMs and platforms in general. In Figure 4 we can see the new platform after the migration, hosting the VMs alongside containerized applications. In the Red Hat Open Virtualization Management part we can find the Red Hat components that provide the automation for Day 2 Operations as well as for application deployment. Red Hat Advanced Cluster Management monitors the VMs and the components of the clusters deployed in the platform and sends the information to Ansible Automation Platform. The applications running on the VMs can be monitored by 3rd party tools (Grafana, Prometheus, etc.) that also send the information to Ansible Automation Platform. If any action needs to be taken to maintain the desired state of any of the infrastructure or the applications, Ansible Automation Platform will trigger a suitable automation process. The automation assets used by it live in the customer’s deployment of Ansible Automation Hub. Finally the automation for the application deployment cycles (CI/CD) is provided by Red Hat OpenShift Application Lifecycle Management.

Ansible Automation Platform can connect to the customer’s ITSM solution, in this way whenever the monitoring triggers a remediation action (Event-Driven Ansible is one of the components of Ansible Automation Platform) a ticket will be opened automatically and the resolution of the incident will be logged in it. Similarly, users can create a ticket to initiate automated actions in a self-service model, for example a developer that needs a VM to test an application.

== What's next
Red Hat offers free programs and trials to get you started.

--
* https://www.redhat.com/en/products/virtualization[*Plan for your VM migration*] 
--

--
* Visit the https://www.redhat.com/en/technologies/cloud-computing/openshift/virtualization/learn[*Red Hat OpenShift Virtualization learning hub*]
--

--
* Download https://developers.redhat.com/products/ansible/overview[*Red Hat Ansible Automation Platform*]
--

== Provide feedback
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/ocp-virt-migration.adoc[issue or submitting a merge request against this Portfolio Architecture product in our GitLab repositories].

The opinions expressed on this website are those of the individual authors and do not necessarily reflect the views of their employer or Red Hat. The content published on this site is contributed by the community and is for informational purposes only. It is not intended to be, and should not be considered as, official Red Hat documentation, support, or advice.



